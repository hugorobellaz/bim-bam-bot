import logging
import json
from datetime import datetime, timedelta

from flask import current_app
from apiai import ApiAI, events
import requests
from mongoengine.queryset.visitor import Q
from celery.signals import celeryd_after_setup

from .models import Candidate, Message, BbjApi, TaxonomyApi, Taxonomy
from . import celery

logger = logging.getLogger(__name__)


@celery.task
def api_callback(response, candidate_id):
    with current_app.app_context():
        if response is not None:
            logger.warning("Received Dialogflow Response " + "-" * 100)
            candidate = Candidate.objects(phone=candidate_id).first()
            was_paused = candidate.isPaused

            # Save Context & Update candidate data
            candidate.update_fields(response)

            if candidate.numberOfFallbacks >= 3:
                # Get Goodbye message from apiai event goodbye
                api = ApiAI(current_app.config["DIALOGFLOW_TOKEN"])

                request = api.event_request(events.Event("GOODBYE"))
                response = request.getresponse()
                response = json.loads(response.read().decode("utf-8"))

            message = response["result"]["fulfillment"]["speech"]

            logger.warning("Candidate is paused: {0} and was paused: "
                           "{1}".format(candidate.isPaused, was_paused))

            if candidate.isPaused and not was_paused:
                # if first paused question, keep in memory paused context
                candidate.set_contexts(response["result"]["contexts"], False)
            elif not candidate.isPaused and was_paused:
                # if fallback paused yes, back to normal
                candidate.contexts = candidate.pausedContexts
                message = candidate.pausedAnswer
            else:
                # set context & paused context
                candidate.set_contexts(response["result"]["contexts"])
                candidate.pausedAnswer = message

            candidate.save()

            if message != "":
                # Create new message
                answer = Message(phone=candidate_id,
                                 message=message,
                                 source="dialogflow")

                answer.send(server=current_app.config["SMS_SERVER"],
                            login=current_app.config["SMS_USERNAME"],
                            key=current_app.config["SMS_KEY"])
                answer.save()
            else:
                logger.warning("Empty Message")
                logger.error(response)
            return True
        return False


@celery.task
def get_intent_async(message, candidate_id):
    api = ApiAI(current_app.config["DIALOGFLOW_TOKEN"])
    # Load Candidate Context
    candidate = Candidate.objects(phone=candidate_id).first()
    contexts = candidate.get_contexts()

    request = api.text_request()
    request.lang = 'fr'
    request.session_id = candidate.phone
    request.query = message
    logger.warning("Message Received: {}".format(message))
    # Add context if existing
    if contexts is not None:
        request.contexts = contexts

    # Get the API answer
    response = request.getresponse()
    response = response.read().decode("utf-8")

    # Load the response
    response = json.loads(response)

    if response["status"]["code"] != 200:
        logger.warning("APIAI failed to respond")
        logger.error(response)
        return None

    return response


@celery.task
def push_candidates():
    api = BbjApi(current_app.config["BBJ_URL"],
                 current_app.config["BBJ_EMAIL"],
                 current_app.config["BBJ_PASSWORD"])

    hour_idle_limit = current_app.config["IDLE_BEFORE_PUSH_H"]
    date_idle_limit = datetime.today() - timedelta(hours=hour_idle_limit)

    # Query all candidate with a filter on created at and updated at
    to_save = Candidate.objects(Q(pushedToBbj=False)
                                & (Q(numberOfFallbacks__gte=3)
                                   | Q(hasFinishedBotProcess=True)
                                   | Q(updatedAt__lte=date_idle_limit))).all()
    logger.warning("Pushing {0:d} candidate to BBJ".format(len(to_save)))
    # For each candidate push to the BBJ back
    for candidate in to_save:
        candidate.push_to_bbj(api)

    # Drop candidate from db if it has been pushed
    # TODO > later when everything works fine do the delete
    # Candidate.objects(pushedToBbj=True).delete()
    return True


@celery.task
def keep_up():
    # do request to homepage to keep heroku up
    requests.get(current_app.config["HEROKU_URL"])
    return True


@celery.task
@celeryd_after_setup.connect
def get_taxonomy_cache(**kwargs):
    logger.info("Caching Taxonomy")
    api = TaxonomyApi(current_app.config["BBJ_TAXONOMY_DB"])
    api.load_taxonomy_in_db()


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(current_app.config["KEEP_UP_RATE_S"],
                             keep_up.s())
    sender.add_periodic_task(current_app.config["BBJ_PUSH_RATE_S"],
                             push_candidates.s())
    sender.add_periodic_task(3600*24, get_taxonomy_cache.s())