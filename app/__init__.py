import os
import logging

from flask import Flask, has_app_context
from flask_mongoengine import MongoEngine
from flask_login import LoginManager
from celery import Celery
import googlemaps

from config import config, Config

basedir = os.path.abspath(os.path.dirname(__file__))
logger = logging.getLogger(__name__)


class FlaskCelery(Celery):
    app = None

    def __init__(self, *args, **kwargs):
        super(FlaskCelery, self).__init__(*args, **kwargs)
        self.patch_task()
        if 'app' in kwargs:
            self.init_app(kwargs['app'])

    def patch_task(self):
        TaskBase = self.Task
        _celery = self

        class ContextTask(TaskBase):
            abstract = True

            def __call__(self, *args, **kwargs):
                if has_app_context():
                    return TaskBase.__call__(self, *args, **kwargs)
                else:
                    with _celery.app.app_context():
                        return TaskBase.__call__(self, *args, **kwargs)

        self.Task = ContextTask

    def init_app(self, app, config):
        self.app = app
        self.config_from_object(config)


db = MongoEngine()
celery = FlaskCelery(__name__, broker=Config.CELERY_BROKER_URL)
gmaps = googlemaps.Client(key=Config.GOOGLE_GEOCODING_KEY)

# Set up Flask-Login
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "auth.login"

from . import tasks


def create_app(config_name="default"):
    logger.warning("Using config {}".format(config_name))
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Setup Extensions
    login_manager.init_app(app)
    db.init_app(app)
    celery.init_app(app, config[config_name])

    # Configure SSL if platform supports it
    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        sslify = SSLify(app)

    # Create app blueprints
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .webhook import webhook as webhook_blueprint
    app.register_blueprint(webhook_blueprint)

    return app
