import logging
import collections
from flask import render_template, redirect, url_for, flash, request
from flask_login import login_required

from . import main
from ..tasks import push_candidates
from ..models import Candidate, Message

logger = logging.getLogger(__name__)


@main.route('/', methods=['GET','POST'])
@login_required
def index():
    candidates = Candidate.objects.order_by("-createdAt", "lastName").all()
    return render_template('candidate-list.html',
                           title="BimBamBot",
                           candidates=candidates)


@main.route('/history/<phone>', methods=['GET','POST'])
@login_required
def history(phone=None):
    messages = Message.objects(phone=phone).order_by("createdAt").all()
    candidate = Candidate.objects(phone=phone).first()
    to_drop = ["hasFinishedBotProcess", "updatedAt",
               "createdAt", "phone", "loc", "source"]
    candidate = candidate.get_cleaned_dict(to_drop)
    candidate = collections.OrderedDict(sorted(candidate.items()))

    return render_template('history.html',
                           title="BimBamBot",
                           messages=messages,
                           candidate=candidate,
                           phone=phone)


@main.route('/contact', methods=['POST'])
@login_required
def contact():
    if request.method == 'POST':
        req = request.values

    if(req is not None) \
            and ('phone' in req)\
            and (req['phone'][0:2] == '33'):

        candidate = Candidate(phone=req['phone'])
        candidate.save()
        res = candidate.init_contact(url=request.url_root + "webhook")

        flash("SMS send to " + req['phone'])
    else:
        flash("Wrong Number Format (try 33687XXXXXX")
    return redirect(url_for('main.index'))


@main.route('/pushtoapi', methods=['GET','POST'])
@login_required
def push_to_api():
    push_candidates()
    return redirect(url_for('main.index'))


@main.context_processor
def utility_processor():
    def clean_date(date=None, format='%d/%m/%Y'):
        if date:
            return date.strftime(format)
        return ""

    def status_to_str(obj):
        return int(obj.get_status() * 100.0)

    return dict(clean_date=clean_date, status_to_str=status_to_str)
