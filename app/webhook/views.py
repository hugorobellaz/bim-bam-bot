import logging
import urllib
from flask import request, current_app

from . import webhook
from ..models import Candidate, Message
from .. import tasks

logger = logging.getLogger(__name__)


@webhook.route('/webhook', methods=['GET', 'POST'])
def webhook():
    raw_query = request.query_string
    enc_query = urllib.parse.parse_qs(raw_query.decode("latin-1"),
                                      encoding="latin-1")
    # Default if utf-8 encoding
    # req = request.args
    # If other encoding "latin-1", get first value of each param:
    req = {k: v[0] for k, v in enc_query.items()}

    source = req.get("source", "sms")

    if "message" not in req \
            or "phoneNumber" not in req\
            or req.get("message") == "":
        logger.warning("Empty data")
        logger.warning(req)
        return "OK"

    # Create Message
    message = Message(phone=req.get("phoneNumber"),
                      message=req.get("message"),
                      source=source)
    message.save()

    # Create candidate
    candidate = Candidate.objects(phone=message.phone).first()

    if not candidate:
        logger.warning("Creating New Candidate {}".format(message.phone))
        candidate = Candidate(phone=message.phone)
        candidate.save()

    # Stop the process if the candidate failed more than 3 times
    if candidate.numberOfFallbacks >= 3 or candidate.hasFinishedBotProcess:
        logger.warning("More than 3 fallback, never answering")
        return "OK"

    # Async
    tasks.get_intent_async.apply_async((message.message, candidate.phone),
                                       link=tasks.api_callback.s(candidate.phone))
    return "OK"

