import logging
import time
import os
import re
import math
from datetime import datetime

import requests
from bson.objectid import ObjectId
from pymongo import MongoClient
from mongoengine.queryset.visitor import Q

from flask_login import UserMixin

from . import db, login_manager, gmaps

logger = logging.getLogger(__name__)


class DialogFlowConverter:

    def update_fields(self, candidate, response):
        if "result" not in response \
                or "action" not in response["result"] \
                or "parameters" not in response["result"]\
                or "metadata" not in response["result"]:
            logger.warning("No action or parameters found")
            logger.error(response)
            return

        action = response["result"]["action"]
        parameters = response["result"]["parameters"]
        metadata = response["result"]["metadata"]

        logger.warning("Action: {}".format(action))
        logger.warning("Parameters: {}".format(parameters))

        if "fallback" in action:
            candidate.numberOfFallbacks += 1
        if "Paused" in action:
            candidate.isPaused = True
        if "endConversation" in metadata and metadata["endConversation"]:
            candidate.hasFinishedBotProcess = True

        if "Invocation" == action:
            pass
        elif "GetName" == action:
            self.update_name(candidate, parameters)
        elif "GetGender" == action:
            self.update_gender(candidate, parameters)
        elif "GetAddress" == action:
            self.update_address(candidate, parameters)
        elif "GetBirthday" == action:
            self.update_birthday(candidate, parameters)
        elif "GetEmail" == action:
            self.update_email(candidate, parameters)
        elif "GetDriverLicence" == action:
            self.update_driver_licence(candidate, parameters)
        elif "GetVehicle" == action:
            self.update_vehicle(candidate, parameters)
        elif "GetContractType" == action:
            self.update_contract_type(candidate, parameters)
        elif "GetStartingDate" == action:
            self.update_starting_date(candidate, parameters)
        elif "GetPublic" == action:
            self.update_public(candidate, parameters)
        elif "GetWorkLocation" == action:
            self.update_work_location(candidate, parameters)
        elif "GetExperienceElderly" == action:
            self.update_experience(candidate, parameters, "elderly")
        elif "GetExperienceDisabled" == action:
            self.update_experience(candidate, parameters, "disabled")
        elif "GetExperienceChildren" == action:
            self.update_experience(candidate, parameters, "children")
        elif "GetExperienceCleaning" == action:
            self.update_experience(candidate, parameters, "cleaning")
        elif "GetFormations" == action:
            self.update_formations(candidate, parameters)
        elif "Paused" in action and "-yes" in action:
            candidate.isPaused = False
        elif "fallback" not in action:
            logger.warning("No field update performed for action: {}"
                           .format(action))
        else:
            logger.warning("Fallback {}".format(action))

    def update_name(self, candidate, parameters):
        if parameters["FirstName"] == "" and parameters["FirstName"] == "":
            candidate.firstName = parameters["anyFirst"]
            candidate.lastName = parameters["anyLast"]
        else:
            candidate.firstName = parameters["FirstName"]
            candidate.lastName = parameters["LastName"]

    def update_gender(self, candidate, parameters):
        gender = parameters["Gender"].lower()
        if gender == "homme":
            candidate.gender = 'M'
        elif gender == "femme":
            candidate.gender = 'F'

    def update_address(self, candidate, parameters):
        address = parameters["address"] + " " + parameters["city"]
        if len(address) > 1:
            geocode_result = gmaps.geocode(address=address,
                                           components={'country': 'FR'})
            if len(geocode_result) > 0:
                geocode_result = geocode_result[0]
                candidate.formattedAddress = geocode_result['formatted_address']
                loc = geocode_result["geometry"]["location"]
                candidate.loc = [loc["lng"], loc["lat"]]
            else:
                logger.warning("Failed to find address")

    def update_birthday(self, candidate, parameters):
        if len(parameters["date"]) > 0:
            try:
                candidate.birthDate = datetime.strptime(parameters["date"],
                                                        "%Y-%m-%d")
            except ValueError:
                logger.warning("Failed to parse date")
        elif len(parameters["number"]) > 0:
            year = int(parameters["number"])
            if 1900 < year < 2100:
                candidate.birthDate = datetime(year, 1, 1)
        return

    def update_email(self, candidate, parameters):
        candidate.mail = parameters["email"]

    def update_driver_licence(self, candidate, parameters):
        if "YesAnswer" in parameters \
                and parameters["YesAnswer"].lower() == "yes":
            candidate.driverLicence = True
        elif "NoAnswer" in parameters \
                and parameters["NoAnswer"].lower() == "no":
            candidate.driverLicence = False

    def update_vehicle(self, candidate, parameters):
        yes_answer = parameters["YesAnswer"].lower()
        no_answer = parameters["NoAnswer"].lower()
        if yes_answer == "yes":
            candidate.vehicle = True
            if candidate.driverLicence is True:
                candidate.maxTransportationDuration = 1800
        elif no_answer == "no":
            candidate.vehicle = False

    def update_contract_type(self, candidate, parameters):
        contracts = []
        cont_dict = Taxonomy.get_all_ids(taxo_type="contract")
        for contract in parameters["ContractType"]:
            if contract.lower() == "cdd":
                contracts.append(cont_dict["cdd"])
            elif contract.lower() == "cdi":
                contracts.append(cont_dict["cdi"])
            elif contract.lower() == "vacations":
                contracts.append(cont_dict["vacation"])
            elif contract.lower() == "tout":
                contracts = [cont_dict["cdd"],
                             cont_dict["cdi"],
                             cont_dict["vacation"]]
                break
            elif contract.lower() == "rien":
                return
        candidate.contractType = contracts

    def update_starting_date(self, candidate, parameters):
        if parameters["date"] != "":
            try:
                candidate.dispoFrom = datetime.strptime(parameters["date"],
                                                        "%Y-%m-%d")
            except ValueError:
                logger.warning("Failed to parse date")
        return

    def update_public(self, candidate, parameters):
        publics = []
        public_dict = Taxonomy.get_all_ids(taxo_type="public")
        for public in parameters['Public']:
            if public == "1":
                publics.append(public_dict["personnes-agees"])
            elif public == "2":
                publics.append(public_dict["personnes-handicapees"])
            elif public == "3":
                publics.append(public_dict["enfants"])
            elif public == "4":
                publics.append(public_dict["entretien-menage"])
            elif public.lower() == "tout":
                publics.append(public_dict["personnes-agees"])
                publics.append(public_dict["personnes-handicapees"])
                publics.append(public_dict["enfants"])
                publics.append(public_dict["entretien-menage"])
                break
            elif public.lower() == "rien":
                publics = []
                break
            else:
                pass
        candidate.public = publics

    def update_work_location(self, candidate, parameters):
        location = []
        db_dict = Taxonomy.get_all_ids(taxo_type="type")
        for loc in parameters['WorkLocation']:
            if loc == "1":
                location.append(db_dict["a-domicile"])
            elif loc == "2":
                # TODO Take all other as default
                all_other = dict(db_dict)
                all_other.pop("a-domicile", None)
                location += list(all_other.values())
            else:
                pass
        candidate.employerType = location
        return

    map_job_pop = {"elderly": "auxiliaire-de-vie-sociale",
                   "disabled": "aide-médico-psychologique",
                   "children": "garde-d-enfants",
                   "cleaning": "aide-ménagère"}

    def update_experience(self, candidate, parameters, population):
        if len(parameters['date-period']) > 0:
            db_dict = Taxonomy.get_all_ids(taxo_type="job")

            period = parameters['date-period'].split("/")
            try:
                from_date = datetime.strptime(period[0], "%Y-%m-%d")
                to_date = datetime.strptime(period[1], "%Y-%m-%d")
                days = (to_date - from_date).days

                exp = {"durationMonths": math.ceil(days/30.41),
                       "experienceTotal": days,
                       "jobId": db_dict[self.map_job_pop[population]]}
                candidate.experiences.append(exp)
            except ValueError:
                logger.warning("Failed to parse period")

    def update_formations(self, candidate, parameters):
        formations = []
        db_dict = Taxonomy.get_all_ids(taxo_type="formation")
        for value in parameters["Formation"]:
            try:
                formations.append(db_dict[value])
            except KeyError:
                logger.warning("Didn't found key: {}".format(value))
                pass
        candidate.formations = formations


class Candidate(db.Document):

    # Mongo DB fields automatically update
    createdAt = db.DateTimeField()
    updatedAt = db.DateTimeField()
    phone = db.StringField(max_length=15, required=True, unique=True)

    # Mongo DB fields update by the bot, in the same order as asked
    firstName = db.StringField(max_length=100, null=False)
    lastName = db.StringField(max_length=100, null=False)
    formattedAddress = db.StringField(default="Ile de France")
    loc = db.ListField(default=[2.349014, 48.864716])
    birthDate = db.DateTimeField()
    mail = db.StringField()
    driverLicence = db.BooleanField()
    vehicle = db.BooleanField()
    gender = db.StringField(default="M")

    # Corresponds to CDD, CDI, Vacation
    contractType = db.ListField()
    dispoFrom = db.DateTimeField(default=datetime.today())
    # Corresponds to Enfants, personnes âgées, ...
    public = db.ListField()
    # Corresponds to Domicile, structure
    employerType = db.ListField()

    experiences = db.ListField()
    formations = db.ListField()
    maxTransportationDuration = db.IntField()

    # Fields set to default value and not updated by the bot
    publicTransport = db.BooleanField()
    exeptionalDispo = db.ListField()
    skill = db.ListField()
    employerType = db.ListField()
    dispoTo = db.DictField(null=True)
    # TODO Remove Object ID
    source = db.ObjectIdField(default="58131c310fd10deb5127c752")
    disponibility = db.ListField()
    job = db.ListField()

    # New fields added
    contexts = db.DynamicField(default=[])
    pausedContexts = db.DynamicField(default=[])

    pausedAnswer = db.StringField()
    pushedToBbj = db.BooleanField(default=False)
    hasFinishedBotProcess = db.BooleanField(default=False)
    isPaused = db.BooleanField(default=False)

    # Other attributes
    numberOfFallbacks = db.IntField(default=0)
    converter = DialogFlowConverter()
    properties_to_drop = ["pausedAnswer", "pushedToBbj",
                          "pausedContexts", "numberOfFallbacks",
                          "contexts", "isPaused", "_id",
                          "formations", "experiences"]

    def __init__(self, phone=None, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        if phone is not None:
            self.phone = phone

    def set_contexts(self, contexts, set_paused=True):
        new_contexts = []
        for context in contexts:
            new_contexts.append(self.__clean_key(context, ".", "|"))
        self.contexts = new_contexts
        if set_paused:
            self.pausedContexts = new_contexts

    def get_contexts(self):
        contexts = []
        for context in self.contexts:
            contexts.append(self.__clean_key(context, "|", "."))

        return contexts

    def update_fields(self, response):
        self.converter.update_fields(self, response)

    def init_contact(self, url):
        payload = dict(phoneNumber=self.phone,
                       message="Bonjour",
                       source="manual")
        try:
            requests.get(url, params=payload)
        except:
            logger.warning("Failed to reach webhook")

    def get_status(self):
        return (len(self.get_cleaned_dict()) - 5) / 26

    def save(self, *args, **kwargs):
        if not self.createdAt:
            self.createdAt = datetime.now()
        self.updatedAt = datetime.now()
        return super(db.Document, self).save(*args, **kwargs)

    def push_to_bbj(self, bbjapi):
        # Do some cleaning on the profile to fit bbj api
        candidate = self.get_cleaned_dict()
        # Push through api
        result = bbjapi.put("/candidates", candidate)
        self.pushedToBbj = result
        # Save Candidate
        self.save()
        return True

    def get_cleaned_dict(self, add_drop=None):
        candidate = self.to_mongo().to_dict()
        to_drop = self.properties_to_drop
        if add_drop is not None:
            to_drop += add_drop

        for key in to_drop:
            candidate.pop(key, None)
        return candidate

    def __clean_key(self, dictionary, to_replace, replace_by):
        new_dict = {}
        for k, v in dictionary.items():
            if type(v) == dict:
                nk = k.replace(to_replace, replace_by)
                new_dict[nk] = self.__clean_key(v, to_replace, replace_by)
            else:
                nk = k.replace(to_replace, replace_by)
                new_dict[nk] = v

        return new_dict


class Message(db.Document):
    phone = db.StringField(max_length=50, required=True)
    message = db.StringField(max_length=500)
    source = db.StringField(max_length=50)
    createdAt = db.DateTimeField()

    def __init__(self, phone, message, source, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        self.phone = phone
        self.message = message
        self.source = source

    def send(self, server, login, key):
        # Send trough all my sms
        if key is not None:
            payload = self.get_payload(login, key)
            requests.get(server, params=payload)

            # Save SMS id
            return "OK"
        else:
            # Save SMS id
            return "DEV00"

    def get_payload(self, login, key, transmitter=""):
        # As for now, the transmitter is empty, otherwise it wouldn't work
        payload = dict(login=login,
                       apiKey=key,
                       message=self.message,
                       mobile=self.phone,
                       tpoa=transmitter)

        return payload

    def save(self, *args, **kwargs):
        if not self.createdAt:
            self.createdAt = datetime.now()
        return super(db.Document, self).save(*args, **kwargs)


class Admin(db.Document, UserMixin):
    username = db.StringField(max_length=200, unique=True)
    password = db.StringField(max_length=200)
    email = db.StringField(max_length=200)

    def __init__(self, username, password, email, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        self.username = username
        self.password = password
        self.email = email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username


@login_manager.user_loader
def load_user(admin):
    return Admin.objects(username=admin).first()


class BbjApi:
    token = None
    token_timestamp = None
    token_duration = 10
    url = ""
    email = ""
    password = ""

    def __init__(self, url, email, password, token_duration=600):
        self.url = url
        self.email = email
        self.password = password
        self.token_duration = token_duration
        self.token = self.get_token()

    def get_token(self):
        payload = {'email': self.email, 'password': self.password}
        response = requests.post(self.url + "/auth/local/admin", data=payload)
        response = response.json()
        self.token_timestamp = time.time()
        return response["token"]

    def put(self, path, payload):
        logger.warning(payload)
        response = requests.post(self.url + "/api" + path,
                                 data=payload,
                                 headers={"Authorization": 'Bearer '
                                                           + self.token})
        logger.warning(response.status_code)
        if response.status_code in [200, 201]:
            return True
        elif (response.status_code == 401) \
                and (time.time() - self.token_timestamp) > self.token_duration:
            self.token = self.get_token()
            return self.put(path, payload)
        else:
            logger.error(response.text)
            return False


class Taxonomy(db.Document):
    name = db.StringField()
    type = db.StringField()
    slug = db.StringField()
    bbj_id = db.ObjectIdField()

    def __init__(self, doc=None, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        if doc is not None:
            self.name = doc["name"]
            if "slug" not in doc:
                self.slug = self.create_slug(self.name)
            else:
                self.slug = doc["slug"]
            self.type = doc["type"]
            self.bbj_id = doc["_id"]

    def create_slug(self, name):
        name = re.sub("\s|,|'", "-", name.lower())
        name = re.sub("\(|\)|/", "", name)
        name = re.sub("-+", "-", name)
        return name

    @staticmethod
    def get_all_ids(taxo_type):
        taxo_dict = {}
        taxo_db = Taxonomy.objects(Q(type=taxo_type)).all()
        for doc in taxo_db:
            taxo_dict[doc["slug"]] = doc["bbj_id"]
        return taxo_dict


class TaxonomyApi:

    def __init__(self, connexion_str):
        self.client = MongoClient(connexion_str)
        self.taxonomies = self.client["bimbamjob-recette"].taxonomies

    def find_taxonomy(self, taxo_type):
        cursor = self.taxonomies.find({"active": True,
                                       "type": taxo_type,
                                       "parent": None})
        for doc in cursor:
            obj = Taxonomy(doc=doc)
            obj.save()

    def load_taxonomy_in_db(self):
        Taxonomy.drop_collection()
        self.find_taxonomy("contract")
        self.find_taxonomy("formation")
        self.find_taxonomy("public")
        self.find_taxonomy("type")
        self.find_taxonomy("job")

