import logging
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash

from . import auth
from ..models import Admin

logger = logging.getLogger(__name__)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))


@auth.route("/login", methods=["GET", "POST"])
def login():
    if request.method == 'GET':
        return render_template('login.html', title="Login")

    remember = False
    if 'remember' in request.form:
        remember = True

    registered_user = Admin.objects(username=request.form['username']).first()
    if registered_user is None \
            or not check_password_hash(registered_user.password,
                                       request.form['password']):
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('login'))

    login_user(registered_user, remember=remember)
    return redirect(url_for('main.index'))


@auth.route('/account', methods=('GET', 'POST'))
@login_required
def account():
    if request.method == 'GET':
        return render_template('login.html', title="Account")

    registered_user = Admin.objects(username=request.form['username']).first()
    if registered_user is None \
            or not check_password_hash(registered_user.password,
                                       request.form['password']):
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('account'))

    registered_user.password = generate_password_hash(request.form[
                                                          'newpassword'])
    registered_user.save()

    flash('Password Changed')
    return redirect(url_for('main.index'))
