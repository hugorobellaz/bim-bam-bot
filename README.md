This is the repository resulting from Data For Good project for BimBamJob. 
The aim of the project is to build a chatbot to collect resume via SMS from people that don't have internet access.

More info on the project here: https://dataforgood.fr/projects/3_bimbamjob.html

## Code Base
This is essentialy a Flask app that expose a front for monitoring and an API to receive/send SMS, communicate with BBJ back end.

## Tech Stack
SMS provider API and DialogFlow API for Natural Language Processing, MongoDB database, Python hosted on Heroku