import unittest
import requests
from flask import current_app
from app import create_app, db
from app.models import Candidate, Message


class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)
        #db.create_all()

    def tearDown(self):
        self.app_context.pop()

    def test_bonjour_answer(self):
        phone = "33900000000"
        message = "Bonjour"
        response = self.client.get("/webhook?phoneNumber=" + phone +
                                   "&message=" + message)

        query = Message.objects(phone=phone, message=message).all()
        self.assertTrue(True)

