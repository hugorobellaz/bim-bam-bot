import unittest
import time
import socket

from app import create_app, db
from app.models import Candidate, Message


class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        self.app_context.pop()

    def __get_HTTP_header(self):
        header = 'HTTP/1.1 200 OK\n'
        time_now = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
        header += 'Date: {now}\n'.format(now=time_now)
        header += 'Server: Simple-Python-Server\n'
        header += 'Connection: close\n\n'

        return header.encode()

    def test_webhook_return_OK(self):
        response = self.client.get('/webhook')
        self.assertEqual(response.status_code, 200)
        self.assertEqual('OK', response.get_data(as_text=True))

    def test_message_is_saved(self):
        phone = "33900000000"
        message = "Bonjour"
        response = self.client.get("/webhook?phoneNumber=" + phone +
                                   "&message=" + message)

        query = Message.objects(phone=phone, message=message).all()
        self.assertTrue(len(query) > 0)

    def test_new_candidate_send_sms(self):
        self.assertTrue(True)

    def test_existing_candidate_send_sms(self):
        self.assertTrue(True)

    def test_sms_is_sent_back(self):
        # Send first SMS
        phone = "33900000001"
        message = "Bonjour"
        response = self.client.get("/webhook?phoneNumber=" + phone +
                                  "&message=" + message)

        # Listen for response
        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection.settimeout(5)
        connection.bind(('0.0.0.0', 5050))

        responses = []
        while True:
            connection.listen(5)
            client, address = connection.accept()
            response = client.recv(2048).decode()

            # Send HTTP response to GET
            client.send(self.__get_HTTP_header())
            client.close()

            responses.append(response)
            if (phone in response) and ("Bienvenue" in response):
                self.assertTrue(True)
                break

        connection.close()