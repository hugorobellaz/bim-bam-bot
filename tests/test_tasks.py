import unittest
import requests
from flask import current_app
from app import create_app, tasks
from app.models import Candidate, Message


class TasksTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)
        #db.create_all()

    def tearDown(self):
        self.app_context.pop()

    def test_push_candidates(self):
        # Count
        before = Candidate.objects(pushedToBbj=False).count()
        tasks.push_candidates()
        after = Candidate.objects(pushedToBbj=False).count()
        self.assertTrue(before > after)

