import os
import re

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    if os.environ.get('SECRET_KEY'):
        SECRET_KEY = os.environ.get('SECRET_KEY')
    else:
        SECRET_KEY = 'NO_SECRET_KEY_USING_RADNOME'
        print('SECRET KEY ENV VAR NOT SET! SHOULD NOT SEE IN PRODUCTION')

    # Flask related config
    SESSION_TYPE = 'filesystem'
    PORT = int(os.environ.get('PORT', 5000))

    # Mongo Config from URI
    r = r'^mongodb\:\/\/(?P<username>[_\w]+):(?P<password>[\w]+)@(?P<host>[' \
        r'\.\w]+):(?P<port>\d+)/(?P<database>[_\w]+)$'
    regex = re.compile(r)
    match = regex.search(os.environ['MONGODB_URI'])
    mongo_data = match.groupdict()

    MONGODB_HOST = mongo_data["host"]
    MONGODB_PORT = int(mongo_data["port"])
    MONGODB_DB = mongo_data["database"]
    MONGODB_USERNAME = mongo_data["username"]
    MONGODB_PASSWORD = mongo_data["password"]


    # API Config
    DIALOGFLOW_TOKEN = os.environ.get("DIALOGFLOW_TOKEN")
    SMS_SERVER = os.environ.get("SMS_SERVER")
    SMS_USERNAME = os.environ.get("SMS_USERNAME")
    SMS_KEY = os.environ.get("SMS_KEY", None)
    GOOGLE_GEOCODING_KEY = os.environ.get("GOOGLE_GEOCODING_API", None)

    # BBJ config
    BBJ_TAXONOMY_DB = os.environ.get("BBJ_TAXONOMY_DB")
    BBJ_EMAIL = os.environ.get("BBJ_EMAIL")
    BBJ_PASSWORD = os.environ.get("BBJ_PASSWORD")
    BBJ_URL = os.environ.get("BBJ_URL")
    BBJ_PUSH_RATE_S = int(os.environ.get("BBJ_PUSH_RATE_S", 60*60))
    IDLE_BEFORE_PUSH_H = int(os.environ.get("IDLE_BEFORE_PUSH_H", 24*7))

    HEROKU_URL = os.environ.get("HEROKU_URL")
    KEEP_UP_RATE_S = int(os.environ.get("KEEP_UP_RATE_S", 15*60))

    SSL_DISABLE = True

    # Celery config
    CELERY_BROKER_URL = os.environ.get("CLOUDAMQP_URL")
    # Will decrease connection usage
    broker_pool_limit = 1
    # We're using TCP keep-alive instead
    broker_heartbeat = None
    # May require a long timeout due to Linux DNS timeouts etc
    broker_connection_timeout = 30
    # AMQP is not recommended as result backend as it creates lots of queues
    result_backend = None
    # Will delete all celeryev. queues without consumers after 1 minute.
    event_queue_expires = 60
    # Disable prefetching, it's causes problems and doesn't help performance
    worker_prefetch_multiplier = 1


    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    task_default_queue = "development"


class TestingConfig(Config):
    TESTING = True
    SMS_SERVER = "http://localhost:5050/"
    SMS_USERNAME = "test_user_sms"
    SMS_KEY = "test_key_sms"


class ProductionConfig(Config):
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USER = os.environ.get('MAIL_USER')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMIN_EMAIL = os.environ.get('ADMIN_EMAIL', "h.robellaz+bbj@gmail.com")
    SSL_DISABLE = False

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # email errors to the administrators
        import logging
        from logging.handlers import SMTPHandler

        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr="error@bbj.heroku.com",
            toaddrs=[cls.ADMIN_EMAIL],
            subject='[BimBamBot] Application Error',
            credentials=(cls.MAIL_USER, cls.MAIL_PASSWORD),
            secure=None)

        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)


class HerokuConfig(ProductionConfig):
    SSL_REDIRECT = True if os.environ.get('DYNO') else False

    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # handle reverse proxy server headers
        from werkzeug.contrib.fixers import ProxyFix
        app.wsgi_app = ProxyFix(app.wsgi_app)

        # log to stderr
        import logging
        from logging import StreamHandler

        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)


class DockerConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # log to stderr
        import logging
        from logging import StreamHandler

        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'heroku': HerokuConfig,
    'docker': DockerConfig,

    'default': DevelopmentConfig
}
