FROM continuumio/miniconda:latest

ENV FLASK_APP bimbambot.py
ENV FLASK_CONFIG production

#RUN apt-get update \
#  && apt-get install -y python3.6\
#  && cd /usr/local/bin \
#  && ln -s /usr/bin/python3.6 python \
#  && pip install --upgrade pip

RUN adduser --disabled-password bimbambot
USER bimbambot

WORKDIR /home/bimbambot

COPY requirements.txt requirements.txt

RUN conda create -y -n bbj python=3.6 \
    && bash -c 'source activate bbj \
                && pip install -r requirements.txt \
                && pip install honcho'
#/home/bimbambot/.conda/envs/bbj/bin
#RUN python -m venv venv
#RUN venv/bin/pip install -r requirements.txt

COPY app app
COPY bimbambot.py config.py Procfile ./
COPY celery_worker.py ./

# run-time configuration
EXPOSE 5000
#CMD ["source activate bbj && cd && flask run"]
ENTRYPOINT bash -c 'source activate bbj && honcho start'